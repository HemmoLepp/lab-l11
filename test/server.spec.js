const chai = require("chai");
const expect = chai.expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

let server;

const arequest = async (value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if (error) reject(error);
        else resolve(response);
    });
});

describe("Test REST API", () => {
    before("Start server", () => {
        server = app.listen(port);
    });
    it("GET / returns 200, which means server OK", async () => {
        const res = await arequest("http://localhost:3000/");
        expect(res.statusCode).to.equal(200);
    });
    it("POST /add { a: 2, b: 3 } returns 5", async () => {
        const options = {
            method: 'POST',
            url: 'http://localhost:3000/add',
            headers: {'content-type': 'application/json'},
            body: {a: 2, b: 3},
            json: true
        };
        await arequest(options)
            .then((res) => {
                expect(res.body.sum).to.equal(5);
            })
            .catch((res) => {
                expect(true).to.equal(false, "add function failed")
            })
    })
    after("Close server", () => {
        server.close();
    });
});