require('dotenv').config();
const express = require('express');
const port = process.env.PORT;

const app = express();
console.log({ node_env: process.env.NODE_ENV });

// Sever static content on express test
const path = require('path');
const public_path = path.join(__dirname, "..", "public");
console.log({ public_path, __dirname });
app.use("/static", express.static(public_path));

/** Proper JSON REST API Middlewares */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Endpoint 1 GET http://localhost:3000
app.get("/", (req, res) => res.send("welcome"));

// Endpoint 2 POST http://localhost:3000/add { a: 2, b: 3 }
app.post("/add", (req, res) => {
    try {
        console.log({ body: req.body });
        const sum = parseInt(req.body.a) + parseInt(req.body.b);
        res.send({ sum });
    } catch (e) {
        res.sendStatus(500);
    }
});

// Endpoint 3 POST http://localhost:3000/webhook-update
let updateState = 0;
const util = require('util');
app.post("/webhook-update", async (req, res, next) => {
    try {
        if (typeof req.headers['x-gitlab-token'] !== 'string') throw new Error('Gitlab webhook token doesnt meet requirements');
        if (req.headers['x-gitlab-token'] !== process.env.WEBHOOK_TOKEN) throw new Error('Gitlab webhook token doesnt match environment');
        if (req.body.ref !== 'refs/heads/main') throw new Error('only interested in main branch');
        if (updateState === 0) {
            updateState = 1;
            res.json({ msg: 'ok' });
            const exec = util.promisify(require('child_process').exec)
            try {
                const { stdout, stderr } = await exec(process.env.UPDATE_SCRIPT);
                console.log('stdout: ', stdout);
                console.log('stderr: ', stderr);
            } catch (err) {
                console.error(err);
            }
            process.exit(1);
        } else {
            res.status(500).json({ msg: 'update currently on'});
        }
    } catch(e) {
        console.error(e);
        res.status(500).json({ msg: 'failed' });
    }
})

// Endpoit 4 GET http://127.0.0.1:50000/ping
app.get("/ping", (req, res) => res.sendStatus(200));

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
}

if (!module.parent) {
    app.listen(port, () => console.log(`Server listening at: localhost:${port}`))
}
